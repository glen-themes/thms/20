// allow me to save you from the monstrosity of code you are about to experience
// here's what I build on AP ezreal:
// 1. sorcerer's boots
// 2. luden's
// 3. rylai's
// 4. lich bane
// 5. rabadon's
// 6. [situational] zhonya's / void staff / morello

$(document).ready(function(){

    $(".tumblr_preview_marker___").remove();

    // custom tooltips (general)
    $("a[title]").each(function(){
        var tit = $(this).attr("title");
        $(this).attr("data-title",tit);
        $(this).removeAttr("title");
    });

    /*------------------------------------*/
    // slide-down info tab thing
    var thespace = parseInt($(".hargao").css("margin-top")),
        h9h9 = parseInt($(".hargao").height());

    $(".hargao").css("margin-top",-h9h9 + thespace);

    $(".kitty, .peeholder").hover(function(){
        $(".hargao").css("margin-top",thespace);
        $(".hargao").css("opacity","1");
    }, function(){
        $(".hargao").css("margin-top",-h9h9 + thespace);
        $(".hargao").css("opacity","0");
    });

    /*------------------------------------*/
    if(window.location.href.indexOf("/customize") > -1){
        // 'normalize' the description on customization page
        // since heights don't fucking render properly
        // thanks tumblr
        var pudding = parseInt(getComputedStyle(document.documentElement)
                       .getPropertyValue("--Desc-Padding"));
        $(".desc").css("padding",pudding + "px" + " 0");
        $(".descont").css("margin-top",-pudding);

    } else {
        // if on main page, do the thing
        var svw = $(".sidelinks").width();
        $(".sidelinks").width(svw);

        var svh = $(".sidelinks").height();
        $(".desna").height(svh);

        $(".desu").css({"position":"absolute","overflow-y":"auto","overflow-x":"hidden"});

        var sbp = parseFloat(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Description-Scrollbar-Padding"));

        if($(".desc").height() > $(".desu").height()){
            $(".desc").css("padding-right",sbp);

            var wiz = $(".desu").width();
            $(".desu").width(wiz + sbp/4)
        }

        var dw = $(".desu").width();
        $(".customlinks").width(dw);
    }

    // when desc has loaded, show whole sidebar
    $(".leftside").css("visibility","visible");

    /*------------------------------------*/
    // show music player in customization panel
    // since the viewport is much smaller in customize
    if(window.location.href.indexOf("/customize") > -1){
        if($(".glenjams-06").length){
            $(".glenjams-06").css("z-index","7");
        }
    }

    /*------------------------------------*/
    // generate frame around reblogger icon
    // since <img>s can't have pseudo elements
    // we are in 2021 boys
    $(".commenter img").each(function(){
        $(this).wrap("<div class='goldframe'>")
    });

    /*------------------------------------*/
    // load 'reblog' and 'like' SVGs
    fetch("//glen-assets.github.io/core-svgs/freepik_repeat.html")
    .then(rt_svg => {
      return rt_svg.text()
    })
    .then(rt_svg => {
      $(".retweet").html(rt_svg)
    });

    fetch("//glen-assets.github.io/core-svgs/freepik_like.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart").html(like_svg)
    });

    /*------------------------------------*/
    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();

    $(".oui p").each(function(){
        if($(this).parents(".comment_container").next().length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });


    $(".oui").each(function(){
        // remove extra space from first commenter
        // if there is nothing before it
        if(!$(this).find(".comment_container:first").closest(".body").length){
            $(this).find(".comment_container:first").addClass("ccfirst")

        };

        // remove reblog border from last commenter
        $(this).find(".comment_container:last").addClass("cclast");
    });


    // turn npf_chat into a chat post
    $(".npf_chat").addClass("chatholder").removeClass("npf_chat");

    $(".chatholder").each(function(){
        if($(this).attr("data-npf")){
            $(this).find("b").addClass("chat_label");
            $(this).contents().last().wrap("<span class='chat_content'>")
        }
    });
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });

    $(".posts").each(function(){
        // show source if post does not have caption
        if(!$(this).find(".comment_container").length){
            $(this).find(".nosrc").show().css("display","flex")
        }
    });

    $(".nosrc").each(function(){
        if($(this).is(":hidden")){
            $(this).remove()
        }
    });

    // remove space from first reblogger if nothing precedes it
    $(".body").each(function(){
        if(!$(this).prev().length){
            $(this).find(".comment_container:first").css("margin-top","calc(var(--Captions-Gap) / -2)")
        }
    });

    // produce gap between npf photoset and first reblogger
    // if that reblog does not have caption text
    $(".npf_inst").each(function(){
        if($(this).parent().prev(".commenter")){
            $(this).css("margin-top","var(--NPF-Caption-Spacing)")
        }
    });

    // if 1st caption has text, and it's meant to look like a photo post,
    // put npf photoset before the 1st caption
    $(".posts").each(function(){
        var fc = $(this).find(".comment_container").eq(0);
        var fcinst = fc.find(".npf_inst");
        if(fcinst.length){
            if(!fcinst.prev().length){
                if(fcinst.next().length){
                    fcinst.prependTo($(this));
                    fcinst.css("margin-bottom","var(--NPF-Caption-Spacing)")
                    fc.css("margin-top","")
                }
            }
        }

        if($(this).children().first().is(".npf_inst")){
            $(this).children().first().css("margin-top","")
        }
    });


    $(".caption").each(function(){
        // remove empty captions
        if(!$(this).children().length){
            $(this).remove();
        }
    });

    $(".posts br").each(function(){
        // remove unnecessary <br>s
        if(!$(this).prev().length){
            $(this).remove();
        }
    });

    // remove reblogger top whitespace if nothing precedes it
    $(".ccfirst").each(function(){
        if(!$(this).closest(".caption").length){
            if(!$(this).prev().length){
                $(this).css("margin-top","calc(var(--Captions-Gap) / -2)")
            }
        }
    });


    // get line-height and set this as bullet points' height
    var LH = getComputedStyle(document.documentElement)
               .getPropertyValue("--Text-LineHeight"),
        LH_num = parseFloat(LH);

    let root = document.documentElement;
    root.style.setProperty('--LH', LH_num);


	$(".quote-source").each(function(){
        var tt = $(this).html();
        var v = tt.replace(" (via ","");
        $(this).html(v);

        $(this).contents().filter(function(){
            return this.nodeType === 3
        }).wrap('<span/>');

        $(this).find(".tumblr_blog").next().hide();
    });

    $(".quote-source p").each(function(){
        if($(this).find("a[data-title]").length){
            $(this).hide();
        }
    });
});//end ready
