![Screenshot preview of the theme "Wanderlust" by glenthemes](https://64.media.tumblr.com/8523d50f3f93853ef530ec000be6899c/204e22769e3b6d94-e1/s640x960/c24d1cd890ed8ba59f5bf7054e0b5f83e72bfc57.gif)

**Theme no.:** 20  
**Theme name:** Wanderlust  
**Theme type:** Free / Tumblr use  
**Description:** Previously known as ‘Destiny’, this theme has been revamped into ‘Wanderlust’, and features Star Guardian Ezreal from League of Legends.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2016-09-20](https://cdn.discordapp.com/attachments/382037367940448256/1102322507157090314/destiny_thm_gif.gif)  
**Rework date:** 2021-01-14

**Post:** [glenthemes.tumblr.com/post/640305767687274496](https://glenthemes.tumblr.com/post/640305767687274496)  
**Preview:** [glenthpvs.tumblr.com/wanderlust](https://glenthpvs.tumblr.com/wanderlust)  
**Download:** [pastebin.com/r6BSjq5q](https://pastebin.com/r6BSjq5q)  
**Credits:** [glencredits.tumblr.com/wanderlust](https://glencredits.tumblr.com/wanderlust)
